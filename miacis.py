import sys
import logging

def characterMap(mapFile):
  with open(mapFile) as f:
    for line in f:
      yield [int(i, 16) for i in line.strip().split('\t')][:2]

def getFontCount(fontFile):
  import sstruct
  from fontTools.ttLib.sfnt import sfntDirectoryFormat, sfntDirectorySize
  from fontTools.ttLib.sfnt import ttcHeaderFormat, ttcHeaderSize
  with open(fontFile, 'rb') as f:
    data = f.read(sfntDirectorySize)
    assert len(data) == sfntDirectorySize, \
      'Not a TrueType or OpenType font (not enough data)'
  sfntDirectory = sstruct.unpack(sfntDirectoryFormat, data)
  if sfntDirectory['sfntVersion'] == 'ttcf':
    assert ttcHeaderSize == sfntDirectorySize
    ttcHeader = sstruct.unpack(ttcHeaderFormat, data)
    assert ttcHeader['Version'] in (0x00010000, 0x00020000), \
      'unrecognized TTC version 0x%08x' % ttcHeader['Version']
    return ttcHeader['numFonts']
  else:
    assert sfntDirectory['sfntVersion'] in ('\000\001\000\000', 'OTTO', 'true'), \
      'Not a TrueType or OpenType font (bad sfntVersion)'
    return 1


def processFont(fontFile, charMap, fontNumber=0):
  from fontTools import ttLib
  logging.info('Loading font...')
  font = ttLib.TTFont(fontFile, fontNumber=fontNumber)

  codeToChar = font['cmap'].getcmap(3, 1).cmap
  for fromCode, toCode in charMap:
    processStatus = 'Processing U+%04X -> U+%04X...' % (fromCode, toCode)
    if sys.platform.lower().startswith('win'):
      print >> sys.stderr, processStatus, '\r',
    else:
      print >> sys.stderr, processStatus, chr(27) + '[A'

    if not fromCode in codeToChar or not toCode in codeToChar:
      logging.warning('Skipped U+%04X -> U+%04X because at least one of them does not exists.' % (fromCode, toCode))
      continue
    fromName = codeToChar[fromCode]
    toName = codeToChar[toCode]

    glyf = ttLib.getTableModule('glyf')
    toGlyph = glyf.Glyph()
    toGlyph.numberOfContours = -1
    for attr in ('xMin', 'yMin', 'xMax', 'yMax'):
      setattr(toGlyph, attr, getattr(font['glyf'][fromName], attr))
    toGlyphComp = glyf.GlyphComponent()
    toGlyphComp.glyphName = fromName
    toGlyphComp.x = 0
    toGlyphComp.y = 0
    toGlyphComp.flags = glyf.UNSCALED_COMPONENT_OFFSET | glyf.USE_MY_METRICS | glyf.ROUND_XY_TO_GRID
    toGlyph.components = [toGlyphComp]
    font['glyf'][toName] = toGlyph
    font['hmtx'][toName] = font['hmtx'][fromName]
    # TODO: Fix hmtx of glyph which having fromGlyph as its components
    # Need to know who reference fromGlyph first...
    # for theName in mixedGlyph:
    #   theWidth = font['hmtx'][fromName][0] - font['glyf'][fromName].xMax + font['glyf'][theName].xMax
    #   theLsb = font['hmtx'][fromName][1] - font['glyf'][fromName].xMin + font['glyf'][theName].xMin
    #   font['hmtx'][theName] = (theWidth, theLsb)
    # TODO: Fix kern... maybe

  # Clean up last line
  if sys.platform.lower().startswith('win'):
    print >> sys.stderr, ' ' * 32, '\r',
  else:
    print >> sys.stderr, ' ' * 32, chr(27) + '[A'

  logging.info('Font processed successful.')
  return font
