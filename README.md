# Miacis

Miacis is a tool for manipulating glyph mapping in a font. It read a mapping file and copy glyph A to glyph B. I use it for mapping Traditional Chinese characters to Simplified Chinese code cells.

## Dependencies

* [TTX/FontTools](http://sourceforge.net/projects/fonttools/)
  * [NumPy](numpy.scipy.org)

Note: You can put dependencies in `lib` folder if you don't like to make it available globally.