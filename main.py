import sys
import os

import logging
logging.basicConfig(format='%(message)s', level=logging.DEBUG)

ROOT_PATH = os.path.dirname(os.path.abspath(sys.argv[0]))
sys.path.insert(1, os.path.join(ROOT_PATH, 'lib'))

def main():
  import miacis
  if len(sys.argv) >= 4:
    inFile = sys.argv[1]
    outFile = sys.argv[2]
    mapFile = sys.argv[3]
    try:
      fontCount = miacis.getFontCount(inFile)
    except AssertionError:
      fontCount = 0
    for i in range(fontCount):
      charMap = miacis.characterMap(mapFile)
      font = miacis.processFont(inFile, charMap, fontNumber=i)
      logging.info('Generating font...')
      if fontCount > 1:
        _outFile = '{1}-{0}{2}'.format(*(i + 1 ,) + os.path.splitext(outFile))
        # Python 2.5 compatible:
        # _outFile = '%s-%%d%s' % os.path.splitext(outFile)
        # _outFile = _outFile % (i + 1)
        font.save(_outFile)
      else:
        font.save(outFile)
    logging.info('Finished.')

if __name__ == '__main__':
  main()
